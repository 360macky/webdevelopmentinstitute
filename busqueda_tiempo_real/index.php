<?php

include('DB_connection.php');

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Búsqueda en tiempo Real</title>
    <link rel="stylesheet" href="./util/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="./util/dist/css/bootstrap-grid.css" />
    <link rel="stylesheet" href="./util/dist/css/bootstrap-reboot.css" />
    <link rel="stylesheet" href="./util/dist/css/custom.css" />
    <link rel="stylesheet" href="./util/assets/sticky-footer-navbar.css" />
    <link
        rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
        crossorigin="anonymous"
    />

    <script src="./util/js/jquery-3.2.1.min.js"></script>
    <script src="./script.js"></script>
    
  </head>
  <body>
    <nav class="navbar  navbar-light bg-light d-flex justify-content-center align-content-center">
      <a class="navbar-brand text-title font-weight-bold" href="#">
        <i class="fas fa-database"></i> MySQL + PHP
        <i class="fas fa-search"></i>
      </a>
    </nav>
    
    <?php

    if (isset($_POST['insertar'])) {    

        $nombres = $_POST['nombres'];
        $apellidos = $_POST['apellidos'];
        $profesion = $_POST['profesion'];
        $estado = $_POST['estado'];
        $fregis = date('Y-m-d');

        $sql = "INSERT INTO personal 
                    (nombres, apellidos, profesion, estado, fregis)
                VALUES
                    (:nombres, :apellidos, :profesion, :estado, :fregis)";

        $sql = $conn->prepare($sql);

        $sql->bindParam(':nombres', $nombres, PDO::PARAM_STR, 25);
        $sql->bindParam(':apellidos', $apellidos, PDO::PARAM_STR, 25);
        $sql->bindParam(':profesion', $profesion, PDO::PARAM_STR, 25);
        $sql->bindParam(':estado', $estado, PDO::PARAM_STR, 25);
        $sql->bindParam(':fregis', $fregis, PDO::PARAM_STR);

        $sql->execute();

        $lastInsertID = $conn->lastInsertId();

        if ($lastInsertID > 0) {
            echo "
                <div class=\"content alert alert-primary text-center rounded-0\">
                    <i class=\"fas fa-check\"></i>
                    ¡Gracias! El nombre registrado es: $nombres
                </div>
            ";
        } else {
            echo "
                <div class=\"content alert alert-danger  text-center rounded-0\">
                    No aw pueden agregar nuevos datos. Comuníquese con el administrador.
                </div>
            ";

            print_r($sql->errorInfo());
        }

    }
    
    ?>
    <div class="d-flex flex-column align-items-center">
        <div class="card w-25 m-4">
            <form class="card-body" method="POST" action="">
                <div class="card-title  text-center h3">
                    Insertar información
                    <span class="text-primary">
                        PDO
                    </span>
                </div>

            <div class="form-group">
                <label>Nombres</label>
                <input type="text" name="nombres" class="form-control" placeholder="Ingresar nombres" required>
            </div>

            <div class="form-group">
                <label>Apellidos</label>
                <input type="text" name="apellidos" class="form-control" placeholder="Ingresar apellidos" required>
            </div>

            <div class="form-group">
                <label>Profesión</label>
                <input type="text" name="profesion" class="form-control" placeholder="Ingresar profesión" required>
            </div>

            <div class="form-group">
                <label>Estado</label>
                <select name="estado" class="form-control" required>
                    <option disabled selected>Seleccionar Estado</option>
                    <option value="Colombia">Colombia</option>
                    <option value="Argentina">Argentina</option>
                    <option value="Ecuador">Ecuador</option>
                    <option value="Peru">Peru</option>
                    <option value="Brasil">Brasil</option>
                    <option value="Bolivia">Bolivia</option>
                    <option value="Chile">Chile</option>
                </select>
            </div>

            <div class="form-group">
                <label>Fecha</label>
                <input type="date" id="xd" class="form-control" placeholder="Ingresar Estado" required>
            </div>

            <button type="submit" name="insertar" class="btn btn-primary btn-block">
            <i class="fas fa-paper-plane"></i>
            Guardar
            </button>
            </form>
        </div>

        <div class="row">
            <div class="col-12 col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered w-100">
                        <thead class="lead bg-primary text-light text-weight-bold">
                            <th width="20%" class="text-center" colspan="2">
                                <i class="fas fa-id-card d-block"></i>
                                Nombres y apellidos
                            </th>
                            <th width="20%" class="text-center">
                                <i class="fas fa-user-tie d-block"></i>
                                Profesión
                            </th>
                            <th width="20%" class="text-center">
                                <i class="fas fa-flag d-block"></i>
                                Estado
                            </th>
                            </th>
                            <th width="20%" class="text-center">
                                <i class="fas fa-calendar-day d-block"></i>
                                Fecha
                            </th>
                        </thead>
                        <tbody>
                            <?php
                            $sql = 'SELECT * FROM personal';
                            $query = $conn->prepare($sql);
                            $query->execute();
                            
                            $result = $query->fetchAll(PDO::FETCH_OBJ);
                            
                            if ($query->rowCount() > 0) {
                                
                                foreach ($result as $row) {
                                                                        
                                    echo "
                                    <tr id=\"$row->id\" class=\"row-style\">
                                        <td colspan=\"2\"><i>$row->nombres $row->apellidos</i></td>
                                        <td>$row->profesion</td>
                                        <td class=\"text-uppercase\">$row->estado</td>
                                        <td>$row->fregis</td>
                                    </tr>
                                    ";
                                    
                                }

                            } else {
                                echo 'No hay filas';
                            }
                            
                            ?>
                        </tbody>
                    </table>
                    <div class="card">
                        <div class="card-body text-primary text-center">
                            <i class="fas fa-list-ol"></i>
                            <?php
                                echo $query->rowCount();
                            ?>
                            registros encontrados
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  </body>
</html>
