CREATE TABLE `personal`
( `id` INT NOT NULL AUTO_INCREMENT,
  `nombres` VARCHAR(50) NOT NULL,
  `apellidos` VARCHAR(200) NULL DEFAULT NULL,
  `profesion` VARCHAR(150) NULL DEFAULT NULL,
  `estado` VARCHAR(100) NULL DEFAULT NULL,
  `fregis` DATE NULL DEFAULT NULL,
PRIMARY KEY  (`id`)
) ENGINE = MySAM DEFAULT CHARSET=utf8;

INSERT INTO `personal` (`id`, `nombres`, `apellidos`, `profesion`, `estado`, `fregis`)

VALUES
    (1, 'Zoila', 'Nina', 'Sistemas', 'Peru', '2020-03-20'),
    (2, 'Luis', 'Fontis', 'Administrador', 'Argentina', '2020-03-20'),
    (3, 'Maria', 'Cotrina', 'Sistemas', 'Ecuador', '2020-03-21'),
    (4, 'Jenifer', 'Carrillo', 'Analista', 'Chile', '2020-03-21'),
    (5, 'Milagros', 'Ferrer', 'Economista', 'Colombia', '2020-03-16');

ALTER TABLE `personal`
    ADD PRIMARY KEY(`id`);

ALTER TABLE `personal`
	MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;