<?php

class Connection
{
    private $host = "localhost";
    private $dbname = "alumnos";
    private $user = "root";
    private $password = "";
    private $connection = null;

    public function get_connection()
    {
        try {
            $this->connection = new PDO("mysql:host=" . $this->host . ";
                                          " . "dbname=" . $this->dbname,
                                          $this->user,
                                          $this->password
                                        );

            return $this->connection;
        }
        catch(Exception $e) {
            echo $e->getMessage();
        }
        finally {
            $this->connection = null;
        }
    }

}

$conn_object = new Connection();
$conn = $conn_object->get_connection();
