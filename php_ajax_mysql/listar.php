<?php
include './connection.php';

$connection_DB = new Connection();
$cnn = $connection_DB->get_connection();

$sql_query = 'SELECT * FROM producto';
$statement = $cnn->prepare($sql_query);
$value = $statement->execute();

if ($value) {
    while ($result = $statement->fetch(PDO::FETCH_ASSOC)) {
        $data['data'][] = $result;
    }

    echo json_encode($data);
} else {
    echo "¡Error!";
}

$statement->closeCursor();
$connection_DB = null;
