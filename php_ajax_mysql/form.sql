CREATE DATABASE `tienda`;

USE `tienda`;

CREATE TABLE `producto` (
`id_producto` int(11) NOT NULL AUTO_INCREMENT,
`producto` varchar(100) DEFAULT NULL,
`stock` int(12) DEFAULT NULL,
`precio` decimal(18, 2) DEFAULT NULL,
PRIMARY KEY (`id_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

INSERT INTO `producto`
VALUES
    (1, 'Galleta', 200, 0.60),
    (2, 'Frugos', 150, 1.50),
    (3, 'Sublime', 50, 3.50);
