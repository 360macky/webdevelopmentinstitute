<?php

class Connection
{
    private $host = "localhost";
    private $dbname = "tienda";
    private $user = "root";
    private $password = "";
    private $connection = null;

    public function get_connection()
    {
        try {
            $this->connection = new PDO("mysql:host=" . $this->host . ";
                                          " . "dbname=" . $this->dbname,
                                          $this->user,
                                          $this->password
                                        );

            return $this->connection;
        }
        catch(Exception $e) {
            echo $e->getMessage();
        }
        finally {
            $this->connection = null;
        }
    }

}
