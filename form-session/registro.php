<?php

session_start();

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro y Login</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
                           integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
                           crossorigin="anonymous"/>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
                           integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
                           crossorigin="anonymous"/>

</head>
<body>

    <div class="m-5">        
        <div class="card m-2">
            <div class="card-body">
                <div class="card-title h3">Registro de usuarios</div>
                <form action="agregar_usuario.php" method="POST" class="form">
                    <div class="form-group">
                        <input class="form-control" type="text" name="nombre_usuario" id="" placeholder="Ingresa usuario">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" name="password" id="" placeholder="Ingresa la contraseña">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" name="password_2" id="" placeholder="Confirma la contraseña">
                    </div>
                    <input class="btn btn-primary" type="submit" value="Guardar">
                </form>
            </div>
        </div>
        <div class="card m-2">
            <div class="card-body">
                <div class="card-title h3">Login</div>
                <form action="login.php" method="POST" class="form">
                    <div class="form-group">
                        <input class="form-control" type="text" name="nombre_usuario" id="" placeholder="Ingresa usuario">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" name="password" id="" placeholder="Ingresa la contraseña">
                    </div>
                    <div class="form-group">
                        <input class="btn btn-success" type="submit" value="Iniciar sesión">
                    </div>
                </form>
            </div>
        </div>
    </div>


</body>
</html>
