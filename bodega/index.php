<?php

include_once 'connection.php';

if ($_POST) {
    $producto = $_POST['producto'];
    $descripcion = $_POST['descripcion'];
    $data_array = array($producto, $descripcion);

    $sql_add_colors = 'INSERT INTO productos (producto, descripcion) VALUES (?, ?)';
    $sql_query_prepared = $connection_DB->prepare($sql_add_colors);
    $sql_query_prepared->execute($data_array);

    echo "
    <script>
        alert(\"Producto agregado exitosamente\");
    </script>";

    header('location: index.php');

}


?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
                               integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
                               crossorigin="anonymous">
        <title>Bodega</title>

    </head>
    <body>

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
                integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
                crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
                integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
                crossorigin="anonymous"></script>
        
        
        <?php
            
            
            $sql_colors_query = 'SELECT * FROM productos';
            
            $sql_cursor = $connection_DB->prepare($sql_colors_query);
            $sql_cursor->execute();
            
            $result = $sql_cursor->fetchAll();
            
        ?>

        <nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand font-weight-bold" href="#">
                Bodega
            </a>
        </nav>

        <div class="container mt-4">
            <div class="row p-4">
                <div class="col-md-8 text-center font-weight-light h3 font-italic text-muted">Productos disponibles</div>
                <div class="col-md-4 text-center font-weight-light h3 font-italic text-muted">Agregar nuevo producto</div>
            </div>
            <div class="row text-center">
                <div class="col-md-8 d-flex flex-center">
                    <div class="d-flex flex-row flex-wrap">

                        <?php   foreach ($result as $producto):   ?>

                        <div class="card p-3 m-2" style="width: 47%">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <?php echo $producto['producto']; ?>
                                </h5>
                                <h6 class="card-text font-weight-light">
                                    <?php echo $producto['descripcion']; ?>
                                </h6>
                                <a href="#" class="btn btn-success m-3">Comprar</a>
                            </div>
                        </div>

                        
                        
                        <?php endforeach ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <form method="POST" class="form pd-5" action="">
                        <div class="form-group">
                            <input name="producto"
                                   type="text"
                                   class="form-control"
                                   placeholder="Nombre del producto"
                                   spellcheck="false"
                                   required
                                   >
                        </div>
                        <div class="form-group">
                            <textarea name="descripcion"
                                      id=""
                                      cols="30"
                                      rows="5"
                                      class="textarea form-control"
                                      placeholder="Descripción"
                                      spellcheck="false"
                                      required
                                      ></textarea>
                        </div>
                        <div class="">
                            <button class="btn btn-primary btn-block">
                                Agregar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </body>
</html>
