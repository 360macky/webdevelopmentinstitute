<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>PHP</title>
    <link rel="stylesheet" href="./util/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="./util/dist/css/bootstrap-grid.css" />
    <link rel="stylesheet" href="./util/dist/css/bootstrap-reboot.css" />
    <link rel="stylesheet" href="./util/dist/css/custom.css" />
    <link rel="stylesheet" href="./util/assets/sticky-footer-navbar.css" />
    <link
        rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
        crossorigin="anonymous"
    />

    <script src="./util/js/jquery-3.2.1.min.js"></script>







    
  </head>
  <body>
    <!-- navbar-expand-lg -->
    <nav class="navbar  navbar-dark bg-dark d-flex justify-content-center align-content-center">
      <a class="navbar-brand text-title text-primary font-weight-bold" href="#">
        <i class="fas fa-database"></i>
      </a>
    </nav>

    <div class="container">
        <div class="display-4 text-center">
            Consulta de datos
        </div>
        <hr>
        <div class="row">
            <div class="col-12 col-md-12">

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead class="lead">
                            <th width="30%" class="text-center">
                                <i class="fas fa-id-card"></i>
                                Nombres
                            </th>
                            <th width="10%" class="text-center">
                                <i class="fas fa-asterisk"></i>
                                Edad
                            </th>
                            <th width="12%" class="text-center">
                                <i class="fas fa-flag"></i>
                                Estado
                            </th>
                            <th width="15%" class="text-center">
                                <i class="fas fa-calendar-day"></i>
                                Fecha de registro
                            </th>
                        </thead>
                        <tbody>
                            <?php
                            $sql = 'SELECT * FROM personal';
                            $query = $conn->prepare($sql);
                            $query->execute();
                            
                            $result = $query->fetchAll(PDO::FETCH_OBJ);
                            
                            if ($query->rowCount() > 0) {
                                
                                foreach ($result as $row) {
                                                                        
                                    echo "
                                    <tr id=\"$row->id\" class=\"row-style\">
                                        <td><i>$row->name</i></td>
                                        <td>$row->age</td>
                                        <td class=\"text-uppercase\">$row->status</td>
                                        <td>$row->fregis</td>
                                    </tr>
                                    ";
                                    
                                }

                            } else {
                                echo 'No hay filas';
                            }
                            
                            ?>
                        </tbody>
                    </table>
                    <div class="card">
                        <div class="card-body text-primary text-center">
                            <i class="fas fa-list-ol"></i>
                            <?php
                                echo $query->rowCount();
                            ?>
                            registros encontrados
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  </body>
</html>
