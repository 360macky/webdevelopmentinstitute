-- Creación de la base de datos 'php_consultapdo'

CREATE DATABASE php_consultapdo;

-- Creación de la tabla 'personal'

CREATE TABLE `personal`
    ( `id` INT NOT NULL AUTO_INCREMENT,
      `name` VARCHAR(50) NOT NULL,
      `age` INT(11) NOT NULL,
      `status` VARCHAR(100) NOT NULL,
      `fregis` DATE NOT NULL,
      PRIMARY KEY  (`id`)) ENGINE = InnoDB;

INSERT INTO `personal`
       (`id`, `name`, `age`, `status`, `fregis`)
VALUES ('1', 'Juan Carrillo', '34', 'Chile', '2019-08-20'),
       ('2', 'Luis Fontis', '26', 'Argentina', '2019-08-19'),
       ('3', 'Maria Cotrina', '23', 'Ecuador', '2019-08-21'),
       ('4', 'Jenifer Carrillo', '27', 'Peru', '2019-08-21'),
       ('5', 'Milagros Ferrer', '29', 'Colombia', '2019-08-16');

ALTER TABLE `personal`
    ADD PRIMARY KEY(`id`);

ALTER TABLE `personal`
	MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6,
COMMIT;
