<?php

include('DB_connection.php');

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Búsqueda en tiempo Real</title>
    <link rel="stylesheet" href="./util/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="./util/dist/css/bootstrap-grid.css" />
    <link rel="stylesheet" href="./util/dist/css/bootstrap-reboot.css" />
    <link rel="stylesheet" href="./util/dist/css/custom.css" />
    <link rel="stylesheet" href="./util/assets/sticky-footer-navbar.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" />

    <script src="./util/js/jquery-3.2.1.min.js"></script>
    <script>
        $(document).ready(function() {

        });
    </script>
</head>

<body>
    <header>
        <nav class="navbar  navbar-dark bg-primary d-flex justify-content-center align-content-center">
            <a class="navbar-brand text-title font-weight-bold" href="#">
                <i class="fas fa-database"></i>
                MySQL + PHP + jQuery + AJAX
                <i class="fas fa-search"></i>
            </a>
        </nav>

        <nav class="navbar navbar-dark d-flex justify-content-center" style="background-color: #0050cb;">
            <ul class="navbar-nav d-flex flex-row justify-content-between">
                <li class="nav-item mr-5">
                    <a class="nav-link" href="index.php">Búsqueda</a>
                </li>
                <li class="nav-item active mr-5">
                    <a class="nav-link" href="#">Registro <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </nav>
    </header>

    <?php

    if (isset($_POST['insertar'])) {

        
        $name = $_POST['name'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];
        
        $sql_query = "INSERT INTO alumnos (name, phone, email) VALUES ('$name', '$phone', '$email')";
        $result = mysqli_query($conn, $sql_query);

        $last_id = mysqli_insert_id($conn);

        if ($result == 1) {
            header('Location: index.php?option=true');
        } else {
            echo "
            <div class=\"content alert alert-danger  text-center rounded-0\">
                No se pueden agregar nuevos datos. Comuníquese con el administrador.
            </div>
        ";
        }
    }



    ?>

    <div class="container">
        <?php
        if (isset($_GET["option"])) { ?>
            <div class="alert alert-success bg-success text-light text-center">
                <i class="fas fa-check mr-2"></i>
                <strong>¡Hecho!</strong>
                El registro ha sido guardado con exito.
            </div>
        <?php } ?>
        <div class="row d-flex justify-content-center">
            <div class="card w-50">
                <form class="card-body" method="POST" action="">
                    <div class="display-2 text-center text-primary mb-4">
                        <i class="fas fa-user-graduate"></i>
                    </div>
                    <div class="card-title  text-center h3">
                        Registrar nuevo alumno
                    </div>
                    <div class="form-group">
                        <label>Nombres y Apellidos</label>
                        <input type="text" name="name" class="form-control" placeholder="Ingresar nombres y apellidos" required autofocus>
                    </div>

                    <div class="form-group">
                        <label>Teléfono</label>
                        <input type="number" name="phone" class="form-control" placeholder="Ingresar teléfono" required>
                    </div>

                    <div class="form-group">
                        <label>Correo</label>
                        <input type="email" name="email" class="form-control" placeholder="Ingresar correo electrónico" required>
                    </div>

                    <button type="submit" name="insertar" class="btn btn-primary btn-block">
                        <i class="fas fa-paper-plane"></i>
                        Registrar
                    </button>
                </form>
            </div>

        </div>
    </div>

    <footer class="footer bg-light">
        <div class="container text-center text-dark">
            Desarrollado en <a href="https://php.net/">PHP</a>, usando <a href="#">MySQL</a> y con la librería <a href="#">jQuery</a> con tecnología <a href="#">AJAX</a> de JavaScript
        </div>
    </footer>
</body>

</html>