<?php

include('DB_connection.php');

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Búsqueda en tiempo Real</title>
    <link rel="stylesheet" href="./util/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="./util/dist/css/bootstrap-grid.css" />
    <link rel="stylesheet" href="./util/dist/css/bootstrap-reboot.css" />
    <link rel="stylesheet" href="./util/dist/css/custom.css" />
    <link rel="stylesheet" href="./util/assets/sticky-footer-navbar.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" />

    <script src="./util/js/jquery-3.2.1.min.js"></script>
    <script>
        $(document).ready(function() {
            (function($) {
                $('#FiltrarContenido').keyup(function() {
                    var ValorBusqueda = new RegExp($(this).val(), 'i');
                    $('.BusquedaRapida tr').hide();

                    $('.BusquedaRapida tr').filter(function() {
                        return ValorBusqueda.test($(this).text());
                    }).show();
                    
                })
            }(jQuery));
        });

        $(document).ready(function() {
            setTimeout(
                function() {
                    $('.content').fadeOut(1500);
                }, 3000);
        });
    </script>
</head>

<body>
    <header>
        <nav class="navbar  navbar-dark bg-primary d-flex justify-content-center align-content-center">
            <a class="navbar-brand text-title font-weight-bold" href="index.php">
                <i class="fas fa-database"></i>
                MySQL + PHP + jQuery + AJAX
                <i class="fas fa-search"></i>
            </a>
        </nav>

        <nav class="navbar navbar-dark d-flex justify-content-center" style="background-color: #0050cb;">
            <ul class="navbar-nav d-flex flex-row justify-content-between">
                <li class="nav-item active mr-5">
                    <a class="nav-link" href="#">Búsqueda <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item mr-5">
                    <a class="nav-link" href="register.php">Registro</a>
                </li>
            </ul>
        </nav>
    </header>


    <div class="container">
        <?php
        if (isset($_GET["option"])) { ?>
            <div class="alert alert-success bg-success text-light text-center content">
                <i class="fas fa-check mr-2"></i>
                <strong>¡Hecho!</strong>
                El registro ha sido guardado con exito.
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-12 col-md-12">

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-primary text-light text-title">Buscar</span>
                    </div>
                    <input type="text" id="FiltrarContenido" name="FiltrarContenido" class="form-control" placeholder="Alumno" autofocus style="cursor: text !important">
                </div>

                <table class="table table-bordered w-100">
                    <thead class="text-primary text-description">
                        <tr>
                            <th width="10%" class="text-center text-description">
                                <i class="fas fa-hashtag d-block mb-2 mt-2 h4"></i>
                                N°
                            </th>
                            <th width="30%" class="text-center text-description">
                                <i class="fas fa-user-tie d-block mb-2 mt-2 h4"></i>
                                Nombres y Apellidos
                            </th>
                            <th width="30%" class="text-center text-description">
                                <i class="fas fa-phone d-block mb-2 mt-2 h4"></i>
                                Celular
                            </th>
                            </th>
                            <th width="30%" class="text-center text-description">
                                <i class="fas fa-at d-block mb-2 mt-2 h4"></i>
                                Email
                            </th>
                        </tr>
                    </thead>

                    <tbody class="BusquedaRapida">
                        <?php
                        $query = "SELECT * FROM alumnos";
                        $result = mysqli_query($conn, $query);
                        $count = 0;

                        while ($data = mysqli_fetch_assoc($result)) {


                            echo "<tr id=\"$count\">";
                            $count++; ?>
                            <td class="text-center text-monospace text-primary"><?php echo $count; ?></td>
                            <td><?php echo $data["name"]; ?></td>
                            <td>
                                <?php echo $data["phone"]; ?>
                            </td>
                            <td>
                                <a href="mail:<?php echo $data["email"]; ?>"><?php echo $data["email"]; ?></a>
                            </td>
                            <?php echo "</tr>"; ?>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="alert alert-primary bg-primary text-light">
                    <i class="fas fa-infinity mr-2"></i>
                    <?php echo $count ?>
                    registros encontrados
                </div>
            </div>
        </div>
    </div>

    <footer class="footer bg-light">
        <div class="container text-center text-dark">
            Desarrollado en <a href="https://php.net/">PHP</a>, usando <a href="#">MySQL</a> y con la librería <a href="#">jQuery</a> con tecnología <a href="#">AJAX</a> de JavaScript
        </div>
    </footer>
</body>

</html>