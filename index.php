<?php
    // PDO: PHP Data Object
    try {
        $db = new PDO("pgsql:dbname=pdo;host=localhost", "username", "password" );
        echo "PDO connection object created";
    }
    catch(PDOException $e) {
        echo $e->getMessage();
    }

    /* Ejecutar una sentencia preparada vinculando variables de PHP */
    $calorías = 150;
    $color = 'red';
    $gsent = $gbd->prepare('SELECT name, colour, calories
        FROM fruit
        WHERE calories < ? AND colour = ?');
    
    $gsent->bindParam(1, $calorías, PDO::PARAM_INT);
    $gsent->bindParam(2, $color, PDO::PARAM_STR, 12);
    $gsent->execute();

?>